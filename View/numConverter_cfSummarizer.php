
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Labroots Coding Task</title>
        
        <!-- CSS file for club list -->
        <!-- <link rel="stylesheet" type="text/css" href="../css/convertOrSum.css"> -->
        <!-- Fonts available through Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Lora|Roboto+Condensed:400,700|Ubuntu' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet">

        <!-- FormValidation CSS file -->
        <link rel="stylesheet" href="../formValidation/dist/css/formValidation.min.css">

        <?php 
        /*************************************************************************************
        //      Author: Jose Delgado
        *************************************************************************************/

            include_once("../Controller/converterSummarizer.php");
            $conSum = new converterSummarizer();
        ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2> Number to Word Converter / Common Factor Summarizer </h2>
                </div>
            </div>
        </div>
    </head>

    <body>

        <div class="container">
            <!-- Integer form that returns a to string conversion or the sum of the common factors for even numbers -->
        	<form NAME="" ACTION="" METHOD="POST">
                    <!-- Description-->
                    <label FOR="intInput">Enter a single odd integer or two even ones separated by a comma (up to 10 digits per integer):</label>
                    <div class="inputForm">
                        <input ID="intInput" TYPE="text" NAME="theInput" pattern="^\d{1,10}(,\s?\d{1,10})?$" 
                        title="Usage: <int>,<int> or <int>, <int> (Comma & second integer optional. Only one space allowed. Max digits per integer: 10)" required/>

                        <!-- Submit Button -->
                        <input TYPE="submit" NAME="submitBtn" VALUE="Submit"/>
                    </div>
        	</form>
                
            <?php
            
                //Submit button was pressed
                if(isset($_POST['submitBtn']))
                {
                    $result = processInput($_POST['theInput'], $conSum);

                    if(!empty($result) || !is_null($result))
                    {
            ?>
                        <div>
                            <p id="result">The result: <span id="resultText"><?php echo $result ?></span></p>
                        </div>
            <?php
                    }
                }                        
            ?>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- FormValidation plugin and the class supports validating Bootstrap form -->
        <script src="../formValidation/dist/js/formValidation.min.js"></script>
        <script src="../formValidation/dist/js/framework/bootstrap.min.js"></script>
    </body>
</html>

<?php
    function processInput($integerInput, $conSumHandle)
    {
        $stripped = preg_replace('/\s+/', '', $integerInput);
        // $trimmed = trim($stripped, ',');
        // echo "Before: " . $integerInput . "<br>";
        // echo "After: " . /*$trimmed*/ $stripped . "<br>";

        $commaPos = strpos($stripped, ',');

        //There's only one input
        if($commaPos === false) {

            $intInput = intval($stripped);
            // Check that the value is an odd integer
            if ($intInput % 2 == 0) {
                echo "<p class='errorMsg'>For single inputs, please enter an odd number!</p>";
            }
            else{
                $conversion = $conSumHandle->getConversionOrSum($intInput);
                return $conversion;
            }
        }
        //There's two inputs
        else {
            list($numOne, $numTwo) = explode(',', $stripped);
            // echo "<br>Input One: " . $numOne . "<br>";
            // echo "Input Two: " . $numTwo;
            $intOne = intval($numOne);
            $intTwo = intval($numTwo);

            // Make sure they're both even
            if ($intOne % 2 !== 0 OR $intTwo % 2 !== 0) {
                echo "<p class='errorMsg'>Both numbers must be even!</p>";
            }
            else {
                $sumOfCF = $conSumHandle->getConversionOrSum($intOne, $intTwo);
                return $sumOfCF;
            }
        }

    }
?>