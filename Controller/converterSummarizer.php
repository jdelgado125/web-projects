<?php
/*************************************************************************************
//		Author: Jose Delgado
*************************************************************************************/
class converterSummarizer {
	
	private function findFactors($num) {
		$posNumberOfFactors = ceil(sqrt($num));
		$factors = array();

		for ($i = 1; $i <= $posNumberOfFactors; $i++) {

			if ($num % $i == 0) {
				//add to array
				$factors[] = $i;
				$factors[] = $num / $i;
			}
		}

		return $factors;
	}

	private function getSumOfCommonFactors($numOne, $numTwo) {
		$numOneFactors = $this->findFactors($numOne);
		$numTwoFactors = $this->findFactors($numTwo);
		$sum = 0;

		$commonFactors = array_intersect($numOneFactors, $numTwoFactors);
		foreach ($commonFactors as $value) {
			$sum += $value;
		}

		return $sum;
	}

	private function convertNumToWord($num) {
		//Part of the PECL intl package provided with PHP
		$numFormatter = new NumberFormatter("en_US", NumberFormatter::SPELLOUT);

		return $numFormatter->format($num);
	}

	public function getConversionOrSum($numOne, $numTwo = null) {

		//We have even integers, find the sum of common factors
		if($numTwo != null) {
			$sumOfFactors = $this->getSumOfCommonFactors($numOne, $numTwo);

			return $sumOfFactors;
		}
		//We have an odd integer, convert to literal string
		else {
			$convert = $this->convertNumToWord($numOne);

			return $convert;
		}
	}

}

?>